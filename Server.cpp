#include "Server.h"

Server::Server(char* rootPath, int Port)
{
	rootDir = rootPath;
	port = Port;
}

int Server::Start()
{
	WSADATA wsa;

	// start winsock v2
	if (WSAStartup(MAKEWORD(2, 2), &wsa) != NO_ERROR)
		return -1;

	// created listen socket
	listenSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (listenSocket == INVALID_SOCKET)
	{
		WSACleanup();
		return -1;
	}

	// setup server settings
	sockaddr_in serverSettings;
	serverSettings.sin_family = AF_INET;
	serverSettings.sin_addr.s_addr = INADDR_ANY;
	serverSettings.sin_port = htons(port);

	// bind listen socket with server settings (bind to port)
	if (bind(listenSocket, (sockaddr*)&serverSettings, sizeof(serverSettings)) == SOCKET_ERROR)
	{
		closesocket(listenSocket);
		WSACleanup();
		return -1;
	}

	// listen on port 80
	if (listen(listenSocket, SOMAXCONN) == SOCKET_ERROR)
	{
		closesocket(listenSocket);
		WSACleanup();
		return -1;
	}

	// start server thread
	serverThreadHandle = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)ServerThread, (void*)this, 0, 0);

	return 0;
}

void Server::ServerThread(void* classPtr)
{
	Server *s = reinterpret_cast<Server*>(classPtr);

	// server loop
	while (s->isAlive == TRUE)
	{
		char headerBuffer[1024];
		int bytesReceived = 0;

		// get store client details
		struct sockaddr_in clientInfo;
		int clientInfoLen = sizeof(clientInfo);

		// accept client connection and store details
		s->clientSocket = accept(s->listenSocket, (sockaddr*)&clientInfo, &clientInfoLen);

		if (s->clientSocket == INVALID_SOCKET)
		{
			closesocket(s->listenSocket);
			WSACleanup();
			s->SetError(-1);
			s->Kill();
		}

		// receive and copy into buffer
		bytesReceived = recv(s->clientSocket, headerBuffer, 1024, 0);

		CStringA headerString(headerBuffer, bytesReceived);

		// parse header to get requested file
		CStringA fileRequest = s->ParseHeader(headerBuffer, bytesReceived);

		// test if empty request (no specific file requested)
		if (fileRequest.Compare("/") == 0)
			fileRequest.Append("index.html");

		// generate response
		CStringA typeResponse = s->GenerateHeader(fileRequest);

		// send response
		int responseBytesSent = s->SendData(s->clientSocket, (char*)typeResponse.GetBuffer(), typeResponse.GetLength());

		typeResponse.ReleaseBuffer();

		if (responseBytesSent == SOCKET_ERROR)
		{
			closesocket(s->clientSocket);
			closesocket(s->listenSocket);
			WSACleanup();
			s->SetError(-1);
			s->Kill();
		}

		// if callback has been set.. use it
		if (s->cb != NULL)
		{
			s->cb(s, fileRequest.GetBuffer());
			fileRequest.ReleaseBuffer();
		}
		else
		{
			// get file data
			fileData file = s->GetFile(fileRequest.GetBuffer());

			fileRequest.ReleaseBuffer();

			// send data
			int dataBytesSent = s->SendData(s->clientSocket, file.data, file.size);

			if (dataBytesSent == SOCKET_ERROR)
			{
				closesocket(s->clientSocket);
				closesocket(s->listenSocket);
				WSACleanup();
				s->SetError(-1);
				s->Kill();
			}

			// deallocate file data
			delete(file.data);
		}

		// shutdown socket
		shutdown(s->clientSocket, SD_BOTH);

		closesocket(s->clientSocket);
	}
}

void Server::Kill()
{
	isAlive = FALSE;

	Sleep(500);

	if (clientSocket)
	{
		shutdown(clientSocket, SD_BOTH);
		closesocket(clientSocket);
	}

	if (listenSocket)
		closesocket(listenSocket);

	TerminateThread(serverThreadHandle, 0);
}

void Server::SetCallback(void* callbackptr)
{
	cb = reinterpret_cast<recCallback*>(callbackptr);
}

int Server::SendData(SOCKET sock, char* buff, int size)
{
	int len = size;

	while (len > 0)
	{
		int bytesSent = send(sock, buff, len, 0);
		if (bytesSent == SOCKET_ERROR)
			return SOCKET_ERROR;
		else {
			len -= bytesSent;
			buff += bytesSent;
		}
	}

	return 0;
}

CStringA Server::ParseHeader(char* headerbuf, int headerlen)
{
	if (headerlen > 10)
	{
		CStringA header(headerbuf, headerlen);

		int requeststart = 0;
		int requestend = 0;

		// find the first '/' character in the header
		requeststart = header.Find('/');

		if (header.Find('?') != -1)
		{
			// find the first following question mark
			requestend = header.Find('?', requeststart);
		}
		else
		{
			// find the first following space char
			requestend = header.Find(' ', requeststart);
		}

		return header.Mid(requeststart, (requestend - requeststart));
	}

	CStringA header;
	header.Append("404");
	return header;
}

fileData Server::GetFile(char* filename)
{
	fileData returnfile = { NULL, 0 };

 	CStringA filepath;
	filepath.Append(rootDir);
 	filepath.Append(filename);
 	filepath.Replace("/", "\\");

	// if a html file with the the 3 letter extension doesnt exist.. try the full extension
	if (filepath.Find(".htm") != -1 && !PathFileExists(filepath))
		filepath.Replace(".htm", ".html");

	HANDLE fileHandle = CreateFileA(filepath, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);

	if (fileHandle != INVALID_HANDLE_VALUE)
	{
		DWORD bytesRead = 0;

		returnfile.size = GetFileSize(fileHandle, NULL);

		returnfile.data = (char*)malloc(returnfile.size);

		ReadFile(fileHandle, returnfile.data, returnfile.size, &bytesRead, 0);

		if (fileHandle)
			CloseHandle(fileHandle);

		return returnfile;
	}

	if (fileHandle)
		CloseHandle(fileHandle);

	return returnfile;
}

CStringA Server::GenerateHeader(CStringA filereq)
{
	char* headerstart = "HTTP/1.1 200 OK\r\nConnection: close\r\nContent-type: ";
	char* headererr = "HTTP/1.1 404 Not Found\r\nConnection: close";
	char* headerend = "\r\n\r\n";

	CStringA headerString(headerstart);

	if (filereq.Find("404") != -1)
	{
		CStringA errstring(headererr);
		errstring.Append(headerend);
		return errstring;
	}

	if (filereq.Find(".htm") != -1 || filereq.Find(".html") != -1)
	{
		headerString.Append("text/html");

		headerString.Append(headerend);
		return headerString;
	}

	if (filereq.Find(".css") != -1)
	{
		headerString.Append("text/css");

		headerString.Append(headerend);
		return headerString;
	}

	if (filereq.Find(".js") != -1)
	{
		headerString.Append("application/javascript");

		headerString.Append(headerend);
		return headerString;
	}

	if (filereq.Find(".jpg") != -1)
	{
		headerString.Append("image/jpeg");

		headerString.Append(headerend);
		return headerString;
	}

	if (filereq.Find(".ico") != -1)
	{
		headerString.Append("image/x-icon");

		headerString.Append(headerend);
		return headerString;
	}

	if (filereq.Find(".png") != -1)
	{
		headerString.Append("image/jpeg");

		headerString.Append(headerend);
		return headerString;
	}

	if (filereq.Find(".gif") != -1)
	{
		headerString.Append("image/jpeg");

		headerString.Append(headerend);
		return headerString;
	}

	if (filereq.Find(".scss") != -1)
	{
		headerString.Append("text/css");

		headerString.Append(headerend);
		return headerString;
	}

	if (filereq.Find(".woff") != -1 || filereq.Find(".woff2") != -1)
	{
		headerString.Append("application/font-woff");

		headerString.Append(headerend);
		return headerString;
	}


	// treat everything else as an octet-stream
	headerString.Append("application/octet-stream");

	headerString.Append(headerend);
	return headerString;
}