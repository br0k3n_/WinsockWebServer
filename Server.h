#pragma once

#include <winsock2.h>
#include <ws2tcpip.h>
#include <atlstr.h>
#include <stdlib.h>
#include <stdio.h>

#include <windows.h>

#pragma comment (lib, "Ws2_32.lib")

// callback typedef
typedef void(recCallback)(void* classPtr, char* requestedFile);

struct fileData
{
	char* data;
	DWORD size;
};

class Server
{
public:
	Server(char* rootPath, int Port);
	int Start();
	void Kill();
	void SetCallback(void* callbackptr);
	fileData GetFile(char* filename);
	int SendData(SOCKET sock, char* buff, int size);

	SOCKET listenSocket;
	SOCKET clientSocket;

	
private:
	CStringA ParseHeader(char* header, int headerlen);
	CStringA GenerateHeader(CStringA filereq);

	int GetLastError() { return error; }
	void SetError(int err) { error = err; }

	static void ServerThread(void* classPtr);
	HANDLE serverThreadHandle = NULL;

	bool isAlive = TRUE;
	char* rootDir;
	int port = 80;

	recCallback* cb = NULL;

	int error = 0;
};