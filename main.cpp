#include "Server.h"

void callback(void* classPtr, char* requestedFile)
{
	Server* s = reinterpret_cast<Server*>(classPtr);

	printf("Callback was called, file requested: %s\n", requestedFile);

	// get file data
	fileData file = s->GetFile(requestedFile);

	// send data
	int bytesSent = s->SendData(s->clientSocket, file.data, file.size);

	delete(file.data);
}

bool isDir(CStringA path)
{
	if (GetFileAttributes(path) == FILE_ATTRIBUTE_DIRECTORY)
		return TRUE;
	else
		return FALSE;
}

int main(int argc, char* argv[])
{
	printf("Terrible Server by broken_\n\n");

	if (argc > 2)
	{
		if (isDir(argv[1]))
		{
			int port = atoi(argv[2]);

			printf("Jet Fuel Can't Melt Steel Beams..\n\n\n");

			Server* server = new Server(argv[1], port);
			server->SetCallback(callback);

			if (server->Start() == 0)
			{
				printf("Server is up on port %d!!!\n", port);
				printf("Press enter to kill\n");

				while (true)
				{
					if (getchar() == '\n')
					{
						printf("Killing Server!!");

						server->Kill();
						return 0;
					}
				}
			}
		}
		else
			printf("Rootpath is not a directory!\n");
	}
	else
		printf("Must specify a path and port!!\n");

	return 0;
}